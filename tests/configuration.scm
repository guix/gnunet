;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;; 
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-configuration)
  #:use-module (srfi srfi-64)
  #:use-module (gnu gnunet configuration))


(test-begin "test-configuration")

(define test-config (load-configuration "tests/testconf.conf"))

(test-equal 2
            (configuration-ref 'integer test-config "testing" "skew_variance"))
(test-equal "localhost"
            (configuration-ref 'string test-config "testing" "hostname"))

(test-end)
