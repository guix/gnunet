;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;; 
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-fs-uri)
  #:use-module (system foreign)
  #:use-module (srfi srfi-64)
  #:use-module (gnu gnunet common)
  #:use-module (gnu gnunet fs uri))

(test-begin "test-fs-uri")

;; wrap-uri
(test-error 'invalid-arg (wrap-uri %null-pointer))

;; keyword-list->string
(test-equal "" (keyword-list->string '()))
(test-equal "foo bar baz" (keyword-list->string '("foo" "bar" "baz")))

;; parse-uri
(test-error 'invalid-arg (parse-uri ""))
(let ((uri (parse-uri "gnunet://fs/ksk/trek")))
  (test-equal #:ksk (uri-type uri)))

;; make-ksk-uri
(test-error 'invalid-arg (make-ksk-uri-pointer))

(define test-uri (make-ksk-uri "+foo" "bar" "baz"))

;; make-sks-uri

(define test-pk "M2OC987U9LFJHQ8LC9SLCV4Q0ONHJV7FMTFQ2VRPE0M9R9MK5860")
(define test-ns (string->data-pointer test-pk (/ 256 8)))

(test-error 'invalid-arg (make-sks-uri-pointer test-ns ""))

(test-assert (uri? (make-sks-uri test-ns "foo")))

;; uri->string
(test-assert (not (string-null? (uri->string test-uri))))

(test-end)
