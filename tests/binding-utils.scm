;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;; 
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-binding-utils)
  #:use-module (gnu gnunet binding-utils)
  #:use-module (system foreign)
  #:use-module (srfi srfi-64))

(test-begin "test-binding-utils")

(test-equal '(kirk & spock & ahura & sulu & scotty)
            (interleave '& '(kirk spock ahura sulu scotty)))

(define foo-alist '((#:foo . 1) (#:bar . 2) (#:baz . 3)))

;; rassq
(test-equal '(#:bar . 2) (rassq foo-alist 2))
(test-equal #f           (rassq foo-alist 5))

;; rassq-ref
(test-equal #:bar (rassq-ref foo-alist 2))
(test-equal #f    (rassq-ref foo-alist 5))

;; string->pointer*
(test-equal %null-pointer (string->pointer* ""))
(test-equal "foo" (pointer->string (string->pointer* "foo")))

(test-end)
