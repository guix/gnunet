;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;; 
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-fs)
  #:use-module (srfi srfi-64)
  #:use-module (system foreign)
  #:use-module (gnu gnunet common)
  #:use-module (gnu gnunet fs))

(test-begin "test-fs")

(define %block-options (make-block-options 0 1))

;;; <file-information>

(define readme  (file->file-information %null-pointer ; no fs for this test
                                        "README"
                                        %block-options
                                        #:keywords '("manual" "important")
                                        #:index? #t))

(test-equal "README" (file-information-filename   readme))
(test-assert    (not (file-information-directory? readme)))

(define fs-dir (directory->file-information %null-pointer ; no fs for this test
                                            "gnu/gnunet/fs"
                                            %block-options))

(test-equal "gnu/gnunet/fs" (file-information-filename   fs-dir))
(test-assert                (file-information-directory? fs-dir))

(test-end)
