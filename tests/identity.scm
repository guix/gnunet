;;;; -*- mode: Scheme; indent-tabs-mode: nil; fill-column: 80; -*-
;;;; 
;;;; Copyright © 2015 Rémi Delrue <asgeir@free.fr>
;;;; 
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;; 
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-identity)
  #:use-module (srfi srfi-64)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (gnu gnunet identity))

;; struct GNUNET_IDENTITY_ego {
;;     struct GNUNET_CRYPTO_EcdsaPrivateKey *pk;
;;     char *name;
;;     void *ctx;
;;     struct GNUNET_HashCode id;
;; }

(define *test-ego*
  (let* ((len  (+ (* 3 (sizeof ptrdiff_t))
		  (* 16 (sizeof uint32)))) ; sizeof struct GNUNET_HashCode
	 (size (sizeof ptrdiff_t))
	 (endi (native-endianness))
	 (bv   (make-bytevector len 0))
	 (priv (string->pointer "sonic"))
	 (name (string->pointer "screwdriver"))
	 (hash (string->utf8 "oods are odd")))
    (bytevector-sint-set! bv 0 (pointer-address priv) endi size)
    (bytevector-sint-set! bv size (pointer-address name) endi size)
    ;; hash will start with "oods are odd" end continue with zeroes
    (bytevector-copy! hash 0 bv (* 3 size) (bytevector-length hash))
    (wrap-ego (bytevector->pointer bv))))

(test-begin "test-identity")

(test-assert (ego? *test-ego*))
(test-equal "sonic" (pointer->string (ego-private-key *test-ego*)))

(test-end)
