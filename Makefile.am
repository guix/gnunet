# Guile-GNUnet --- Guile bindings for GNUnet.
# Copyright © 2015 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of Guile-GNUnet.
#
# Guile-GNUnet is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Guile-GNUnet is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Guile-GNUnet.  If not, see <http://www.gnu.org/licenses/>.

MODULES =					\
  system/foreign/unions-read-write.scm		\
  system/foreign/unions.scm			\
  gnu/gnunet/common.scm				\
  gnu/gnunet/scheduler.scm			\
  gnu/gnunet/binding-utils.scm			\
  gnu/gnunet/container/metadata.scm		\
  gnu/gnunet/fs.scm				\
  gnu/gnunet/configuration.scm			\
  gnu/gnunet/identity.scm			\
  gnu/gnunet/fs/progress-info.scm		\
  gnu/gnunet/fs/uri.scm

GOBJECTS = $(MODULES:%.scm=%.go) gnu/gnunet/config.go

nobase_dist_guilemodule_DATA = $(MODULES)
nobase_nodist_guilemodule_DATA = $(GOBJECTS)

# TODO: Handle tests.

AM_V_GUILEC = $(AM_V_GUILEC_$(V))
AM_V_GUILEC_ = $(AM_V_GUILEC_$(AM_DEFAULT_VERBOSITY))
AM_V_GUILEC_0 = @echo "  GUILEC" $@;

# Unset 'GUILE_LOAD_COMPILED_PATH' altogether while compiling.  Otherwise, if
# $GUILE_LOAD_COMPILED_PATH contains $(moduledir), we may find .go files in
# there that are newer than the local .scm files (for instance because the
# user ran 'make install' recently).  When that happens, we end up loading
# those previously-installed .go files, which may be stale, thereby breaking
# the whole thing.
#
# XXX: Use the C locale for when Guile lacks
# <http://git.sv.gnu.org/cgit/guile.git/commit/?h=stable-2.0&id=e2c6bf3866d1186c60bacfbd4fe5037087ee5e3f>.
.scm.go:
	$(AM_V_GUILEC)$(MKDIR_P) `dirname "$@"` ;			\
	unset GUILE_LOAD_COMPILED_PATH ;				\
	LC_ALL=C							\
	$(top_builddir)/pre-inst-env					\
	$(GUILD) compile -L "$(top_builddir)" -L "$(top_srcdir)"	\
	  -Wformat -Wunbound-variable -Warity-mismatch			\
	  --target="$(host)"						\
	  -o "$@" "$<"

SUFFIXES = .go

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guix_install_go_files = install-nobase_nodist_guilemoduleDATA
$(guix_install_go_files): install-nobase_dist_guilemoduleDATA

TESTS =						\
  tests/binding-utils.scm			\
  tests/configuration.scm			\
  tests/container-metadata.scm			\
  tests/fs.scm					\
  tests/identity.scm				\
  tests/progress-info.scm			\
  tests/system-foreign-unions.scm		\
  tests/uri.scm

TEST_EXTENSIONS = .scm
SCM_LOG_COMPILER = $(top_builddir)/pre-inst-env $(GUILE)

CLEANFILES = $(GOBJECTS) $(SCM_TESTS:tests/%.scm=%.log)

EXTRA_DIST =                                    \
  system/foreign/unions-read-write.scm          \
  system/foreign/unions.scm
